# Практическая работа по модулю "Инструменты" курса "DevOps-инженер. Основы"

## Цели практической работы
* Познакомиться с пулом инструментов, которые можно внедрить в рамках DevOps-практик. 
* Закрепить знания, полученные в рамках модуля.

## Что входит в задание
1. Для каждого из доменов (`Collaboration`, `Build`, `Test`, `Deploy`, `Run`) подберите пул инструментов, которые вы внедрили бы у себя в компании при необходимости.

## Установка и настройка

1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/basic/module_13.git
   cd module_13
   ```

В компании разрабатываем ETL-процессы для медицинских организаций.

| Collaboration ||
|---------------|---|
| Управление жизненным циклом | **Можно внедрить**. Jira, потому что текущий тасктрекер самописный и крайне неудобен в использовании. |
| Коммуникации | **Уже есть**. Telegram. Чаты, звонки, группы. Используем TG-ботов для запуска заданий в Jenkins. Различная нотификация, например, алерты `sentry` также приходят в TG. |
| Обмен знаниями | **Уже есть**. Confluence. В нем оформляем проектную документацию и ведем свою корпоративную базу знаний. |

| Build ||
|-------|---|
| SCM/VCS | **Уже есть**. Gitlab. Git - самая популярная VCS. Наличие в Gitlab CI из коробки. Наличие semantic release из коробки. |
| CI | **Уже есть**. GitlabCI. Поддерживается Gitlab'ом из коробки, достаточно функционален, декларативная настройка в YAML-формате. |
| Build | **Уже есть**. Jenkins. При помощи webhook срабатывает задача в Jenkins, которая на основе версии по semantic release собирает новый образ Docker и пушит его в репозиторий Nexus. |

| Test ||
|------|---|
| Юнит-тесты | **Уже есть**. Pytest. Повышают качество кода и позволяют обнаружить проблемы на ранних стадиях. |
| Тесты на качество данных | **Можно внедрить**. Гарантируется, что ETL-приложение корректно отклоняет, исправляет неверные данные, заменяет их на значения по умолчанию, или игнорирует и выводит соответствующий отчет. |
| Преобразование данных | **Можно внедрить**. Гарантируется, что все данные преобразуются правильно в соответствии с бизнес-правилами и / или проектной спецификацией. |
| Полнота данных | **Можно внедрить**. Гарантируется, что все ожидаемые данные загружены. |

| Deploy ||
|--------|---|
| Deployment | **Уже есть**. Dokku. Используем для автоматизированного развертывания ETL-приложений из Docker-образов. Удобное масштабирование и линкование микросервисов. |
| Config management/provisioning | **Уже есть**. Ansible. Удобен в использовании, не требует клиента на настраиваемых серверах, огромное количество модулей под различные задачи (можно написать и свой), может использоваться для деплоя с локальной машины. |
| Artifact management | **Уже есть**. Nexus. Там храним версионированные Docker-образы ETL-приложений и модули Python, написанные для корпоративных целей. |

| Run |
|-----|
| ETL-приложения развертываются на собственных корпоративных серверах в виде Docker-контейнера под управление Dokku. |